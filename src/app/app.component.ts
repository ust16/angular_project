import { Component } from '@angular/core';
import { HttpClient} from '@angular/common/http'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  con_data:any
  constructor(private ht:HttpClient){

  }
  fun1(){
    this.ht.get("http://restcountries.com/v3.1/all").subscribe(dt=>{
      this.con_data=dt;
      console.log(this.con_data)
    })
    }
}
